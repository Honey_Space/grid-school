import Head from 'next/head'
import Grid from '../components/grid';

const Playground = () => {
  return (
    <div>
      <Head>
        <title>Playground</title>
      </Head>
      <Grid>

      </Grid>
    </div>
  );
}

export default Playground