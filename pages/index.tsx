import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import Card from '../components/card'
import Grid from '../components/grid'
import styles from '../styles/Home.module.css'

const cardAmount = 10

const Home: NextPage = () => {
  return (
    <div className="h-full w-full">
      <Head>
        <title>Gallery</title>
      </Head>

      {/* general */}
      <Grid className="mb-10">
        <div className="col-span-full text-5xl text-center">
          Общий случай
        </div>
      </Grid>
      <Grid className="mb-10">
        {new Array(cardAmount).fill("1", 0, cardAmount).map((item, inddex) =>
          <Card key={inddex} />)}
      </Grid>

      {/* based on colums */}
      <Grid className="mb-10">
        <div className="col-span-full text-5xl text-center">
          Колонки
        </div>
      </Grid>
      <Grid className="mb-10">
        <div className="col-span-full md:col-start-2 md:col-span-6 lg:col-start-3 lg:col-span-8">
          <Grid paddings={false} className="bg-gray-500 h-16 rounded" customCols={{ xl: 0, sm: 4, md: 6, lg: 8 }}>
            {/* {new Array(8).fill("1", 0, 8).map(item =>
              <div className="bg-pink-extra-light col-span-1 h-16" />)} */}
          </Grid>
        </div>
      </Grid >
      <Grid>
        <div className="col-span-full md:col-start-2 md:col-span-6 lg:col-start-3 lg:col-span-8 px-8">
          <Grid paddings={false} className="bg-gray-500 h-16 rounded" customCols={{ xl: 0, sm: 4, md: 6, lg: 8 }}>
            {/* {new Array(8).fill("1", 0, 8).map(item =>
              <div className="bg-pink-extra-light col-span-1 h-16" />)} */}
          </Grid>
        </div>
      </Grid >
    </div >
  )
}

export default Home
