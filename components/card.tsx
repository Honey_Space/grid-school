const Card = () => {
  return (
    <div className="bg-white text-black rounded px-6 py-8 flex flex-col items-center col-span-4">
      <div className="h-44 bg-gray-400  rounded w-full mb-4">

      </div>
      <div className="text-4xl mb-4">
        Название
      </div>
      <div className="mb-4">
        <div className="text-base text-center -mb-2">
          Какая-то штука
        </div>
        <div className="text-xl">
          Какое-то значение
        </div>
      </div>
      <div className="mb-4">
        <div className="text-base text-center -mb-2">
          Какая-то штука
        </div>
        <div className="text-xl">
          Какое-то значение
        </div>
      </div>
      <div className="mb-4">
        <div className="text-base text-center -mb-2">
          Какая-то штука
        </div>
        <div className="text-xl">
          Какое-то значение
        </div>
      </div>
      <button className="border border-black px-4 py-1 rounded hover:bg-gray-300 transition-all">
        Кнопка
      </button>
    </div>
  );
}

export default Card