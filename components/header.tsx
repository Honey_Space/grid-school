import Link from "next/link"
import Grid from "./grid";

const Header = () => {
  return (
    <Grid>
      <div className="col-span-full flex -mx-3 py-6 text-xl">
        <Link href="/">
          <div className="mx-3 px-2 py-1 cursor-pointer hover:text-pink-dark transition-all">
            Gallery
          </div>
        </Link>
        <Link href="/playground">
          <div className="mx-3 px-4 py-1 cursor-pointer hover:text-pink-dark transition-all">
            Playground
          </div>
        </Link>
      </div>
    </Grid>
  );
}

export default Header