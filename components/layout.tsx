import Header from "./header";

const Layout = ({ children }: { children: any }) => {
  return (
    <div className="pb-32">
      <Header />
      {children}
    </div>
  );
}

export default Layout