import { MutableRefObject, useRef, useState } from "react";

function clearSelection() {
  if (window.getSelection) {
    window.getSelection()?.removeAllRanges();
  }
}

const cols = "grid-cols-0 grid-cols-1 grid-cols-2 grid-cols-3 grid-cols-4 grid-cols-5 grid-cols-6 grid-cols-7 grid-cols-8 grid-cols-9 grid-cols-10 grid-cols-11 grid-cols-12"
const cols2 = "md:grid-cols-0 md:grid-cols-1 md:grid-cols-2 md:grid-cols-3 md:grid-cols-4 md:grid-cols-5 md:grid-cols-6 md:grid-cols-7 md:grid-cols-8 md:grid-cols-9 md:grid-cols-10 md:grid-cols-11 md:grid-cols-12"
const cols3 = "lg:grid-cols-0 lg:grid-cols-1 lg:grid-cols-2 lg:grid-cols-3 lg:grid-cols-4 lg:grid-cols-5 lg:grid-cols-6 lg:grid-cols-7 lg:grid-cols-8 lg:grid-cols-9 lg:grid-cols-10 lg:grid-cols-11 lg:grid-cols-12"
const cols4 = "xl:grid-cols-0 xl:grid-cols-1 xl:grid-cols-2 xl:grid-cols-3 xl:grid-cols-4 xl:grid-cols-5 xl:grid-cols-6 xl:grid-cols-7 xl:grid-cols-8 xl:grid-cols-9 xl:grid-cols-10 xl:grid-cols-11 xl:grid-cols-12"

type CustomCols = number | {
  sm: number,
  md: number,
  lg: number,
  xl: number,
}

const getClassName = (cols: CustomCols) => {

  if (typeof cols === "number")
    return `grid-cols-${cols}`

  let output = ""

  for (let key of Object.keys(cols)) {
    if (key !== 'sm')
      // @ts-ignore
      output += `${key}:grid-cols-${cols[key]} `
  }

  return output
}

const Grid = ({ children, outlines = false, paddings = true, className = "", customCols, gridClassName = "" }:
  { children: any, outlines?: boolean, paddings?: boolean, className?: string, gridClassName?: string, customCols?: CustomCols }) => {

  const [showOtlines, setShowOtlines] = useState(outlines)
  const containerRef = useRef() as MutableRefObject<HTMLDivElement>
  const zRef = useRef() as MutableRefObject<HTMLDivElement>
  const gridRef = useRef() as MutableRefObject<HTMLDivElement>

  return (
    <div ref={containerRef} className={`relative isolate ${showOtlines ? "bg-purple-dark" : ""} ${className}`} onClick={(e) => {
      console.log(e.target)
      if (e.nativeEvent.shiftKey && e.nativeEvent.ctrlKey && (e.target === containerRef.current || e.target === gridRef.current || e.target === zRef.current)) {
        setShowOtlines(!showOtlines)
        clearSelection()
      }
    }}>
      <div ref={gridRef} className={`${showOtlines ? "opacity-70" : ""} h-full w-full xl:max-w-6xl ${paddings ? "px-4 md:px-12 lg:px-16 xl:px-0" : ""} mx-auto grid gap-4 ${customCols ? getClassName(customCols) : "grid-cols-4 md:grid-cols-8 lg:grid-cols-12"} ${gridClassName}`}>
        {children}
      </div>
      {showOtlines && <div ref={zRef} className="h-full w-full absolute top-0 left-0 -z-1 ">
        <div className={`h-full w-full xl:max-w-6xl ${paddings ? "px-4 md:px-12 lg:px-16 xl:px-0" : ""} mx-auto grid gap-4 ${customCols ? getClassName(customCols) : "grid-cols-4 md:grid-cols-8 lg:grid-cols-12"}  bg-purple ${gridClassName}`}>
          {customCols === undefined ? <>
            <div className="bg-purple-extra-light" />
            <div className="bg-purple-extra-light" />
            <div className="bg-purple-extra-light" />
            <div className="bg-purple-extra-light" />

            <div className="bg-purple-extra-light hidden md:block" />
            <div className="bg-purple-extra-light hidden md:block" />
            <div className="bg-purple-extra-light hidden md:block" />
            <div className="bg-purple-extra-light hidden md:block" />

            <div className="bg-purple-extra-light hidden lg:block" />
            <div className="bg-purple-extra-light hidden lg:block" />
            <div className="bg-purple-extra-light hidden lg:block" />
            <div className="bg-purple-extra-light hidden lg:block" />
          </> :
            (
              typeof customCols === "number" ?
                new Array(customCols).fill("1", 0, customCols).map((item, inddex) =>
                  <div key={inddex} className="bg-purple-extra-light" />)
                :
                <>
                  {new Array(customCols.sm).fill("1", 0, customCols.sm).map((item, inddex) =>
                    <div key={inddex} className="bg-purple-extra-light" />)}

                  {new Array(customCols.md - customCols.sm).fill("1", 0, customCols.md - customCols.sm).map((item, inddex) =>
                    <div key={inddex} className="bg-purple-extra-light  hidden md:block" />)}

                  {new Array(customCols.lg - customCols.md).fill("1", 0, customCols.lg - customCols.md).map((item, inddex) =>
                    <div key={inddex} className="bg-purple-extra-light hidden lg:block" />)}

                  {customCols.xl > 0 && new Array(customCols.xl - customCols.lg).fill("1", 0, customCols.xl - customCols.lg).map((item, inddex) =>
                    <div key={inddex} className="bg-purple-extra-light hidden xl:block" />)}
                </>
            )
          }
        </div>
      </div>}
    </div>
  );
}

export default Grid