module.exports = {
  purge: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      zIndex: {
        "-1": -1,
      },
      colors: {
        "pink-dark": "#933a63",
        pink: "#ff95b1",
        "pink-light": "#ffc4cf",
        "pink-extra-light": "#ffecf0",
        "purple-dark": "#44387a",
        purple: "#7679db",
        "purple-light": "#96b3fd",
        "purple-extra-light": "#d4d5f6",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
